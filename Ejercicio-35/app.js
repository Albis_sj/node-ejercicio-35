// 35.- Recibir argumentos para sumar, restar, multiplicar y dividir del 1 al 100:
// • Sin yargs.
// • Con yargs.
// Realizar la escritura en un archivo txt, dentro de una carpeta llamada salida-operaciones que está en la raíz del proyecto.

// Subir al repositorio de GitLab.


//importacion de archivos abajo
const { generarResta } = require('./helpers/operaciones')
const { generarArchivos } = require('./helpers/operaciones')
const { generarMultiplicacion } = require('./helpers/operaciones')
const { generarDivision } = require('./helpers/operaciones')

//const base = 5;

//console.log(process.argv);

const [, , arg3 = 'base=7'] = process.argv;
const [, base] = arg3.split('=')
console.log(base);

generarArchivos(base)
generarResta(base)
generarMultiplicacion(base)
generarDivision(base)